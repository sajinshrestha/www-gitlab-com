---
layout: markdown_page
title: "Calendar Year 2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. Sales prospects. Marketing achieves the SCLAU plan. Measure pipe-to-spend for all marketing activities.

* VPE
  * Support: Improve Support expectations and documentation for Proof of Concept and Trial prospects, including temporary expanded Support focus for top 10 prospects (defined by Sales).
      * Amer East: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * Amer West: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * APAC: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
      * EMEA: 95% SLA for Premium/Ultimate/Gitlab.com customers and 95% CSAT for all customers
* CMO: Marketing achieves the SCLAU plan. 8 SCLAU per SDR per month, content 2x QoQ, website information architecture helps 50% QoQ.
  * Product Marketing: Complete Information Architecture for customer facing web site pages and add 50 product comparison pages.
  * Product Marketing: Deliver 15 enablement sessions each to sales and xDR teams - 30 total sessions.
  * Product Marketing: Target 6 industry analyst reports to include GitLab coverage.
  * MOps: Implement top three personas into our behavioural and demographic scoring model to get more qualified leads to BDRs or SDRs. 
  * Online Growth: Use data-driven approach and SEO/PPC/ABM/Paid social channels to drive traffic to high intent assets and increase signups for gated assets trials, webinars, and demos by 25% compared to last quarter.n
  * MPM: Support all field events with pre-event targeting, confirmations, sales notifications, and reminders to drive a minimum of 90% utilization per event.
  * MPM: Triple our marketable (aka opted-in for communications) database in Salesforce and Marketo by improving opt-in practices, revamping email communication positioning, and implementing strategic opt-in campaign to engage the database. 
  * MPM: Launch an on-demand product demo and a weekly LIVE product Q&A session, generating 5x higher net new leads than previous weekly Enterprise Demo.
  * Content Marketing: Increase sessions on content marketing blog posts 5% MoM.
      * October: 55,583 sessions
      * November: 58,362 sessions
      * December: 61,280 sessions
* CMO: Measure pipe-to-spend for all marketing activities. Content team based on it, link meaningful conversations to pipe, double down on what works.
  * Online Growth: Improve measurement of pipe-to-spend with more granular data.  Work to expand reporting beyond initial pipe-to-spend calculation and ensure online actions are measured and reported.
  * Online Growth: Ensure site is properly tagged for attribution and optimized for crawlers and content and assets are aligned to Information Architecture.
  * MOps: Restructure Marketo/Bizible/SFDC to align the campaign & progression statuses across the system to allow for better multi-touch attribution tracking.
  * MOps: Streamline record management processes. Refresh SFDC layouts with unnecessary fields removed, create record views
* VP Alliances: Establish differentiated relationships with Major Clouds. GitLab as a Service LoI on a major cloud, 500+ leads through joint marketing, GTM and Partner agreement in place tihe a large private cloud provider.
* VP Alliances: establish GitLab as DevOps tool for CNCF.  Three keynote/speaking engagements at CNCF events and 1000+ leads from K8s partners
* CFO: Use data to drive business decisions
  * Director of Business Operations: Create scope for User Journey that is documented and mapped to key data sources. *50%*
  * Director of Business Operations: Top of funnel process and metrics defined and aligned with bottom of funnel. *70%*
    * Data & Analytics: Able to define and calculate Customer Count, MRR and ARR by Customer, Churn by Cohort, Reason for Churn *75%* - Some metrics still require [review](https://about.gitlab.com/handbook/business-ops/data-quality-process/)
  * Director of Business Operations: Looker Explores generated for Customer Support, PeopleOps, GitLab.com Event Data, Marketing Data *100%*
    * Data & Analytics: Single Data Lake for all raw and transformed company data - migration to Snowflake compelte with DevOps workflow in place, GitLab.com production data extracted and modelled *75%*
    * Data & Analytics: Configuration and Business processes documented with integrity tests that sync back with upstream source (SFDC), dbt docs deployed *100%*
* Product: Listen to and prioritize top feature requests from customers through sales / customer success. 10 features prioritized. => **130% Complete** [13 items prioritized](https://docs.google.com/document/d/1dAs9HoAbuiRzYYvk-AFMggbN-1Hb1l6L17jMzLePbkA/edit#heading=h.qnnbz6f7mo5y)

### CEO: Popular next generation product. Finish 2018 vision of entire DevOps lifecycle. GitLab.com is ready for mission critical applications. All installations have a graph of DevOps score vs. releases per year.

* VPE: Make GitLab.com ready for mission critical customer workloads => 70%, meeting our business goals but by no means perfect, lots of automation to do.
  * Frontend:
    * Spend no more than 15 point of our [error budget]: 0/15
    * Convert a Vue app to use GraphQL as a side-by-side feature with user opt-in
    * Create, Plan:
      * Spend no more than 15 point of our [error budget]: 0/15
      * 3 charts in gitlab-ui
    * Distribution, Monitor and Packaging:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Rewrite and integrate 4 CSS components that align with our design system into GitLab
  * Dev Backend:
    * Adopt in collaboration with Product a consistent process for prioritizing
      work across all backend teams (x/13)
    * Perform 3 iterations on [retrospective](/handbook/engineering/workflow/#retrospective) process to be more effective for Engineering (1/3)
    * Gitaly:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Deliver first iteration of Object Deduplication work
    * Gitter:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Shut down billing and embeds on `apps-xx`
    * Plan:
      * Spend no more than 15 point of our [error budget]: 0/15 (100%)
      * Complete [phase 1 of preparedness for Elasticsearch on GitLab.com] (50%)
    * Create:
      * Spend no more than 15 point of our [error budget]: 0/15
    * Manage:
      * Spend no more than 15 point of our [error budget]: 15/15 [#536](https://gitlab.com/gitlab-com/gl-infra/production/issues/536)
    * Geo:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Complete all issues in Phase 1 of the DR/HA plan working with the Production Team ()
    * Distribution:
      * Spend no more than 15 point of our [error budget]: 0/15
      * [Automate library upgrades](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/194) across distributed projects
  * Ops Backend:
    * Drive the implementation of a throughput dashboard for each development team and conduct a training for each: 13/13 dashboards, 1/1 practice training, 3/3 group training (100%)
    * Spend no more than 15 point of our [error budget]: 0/15
    * Configure:
      * Spend no more than 15 point of our [error budget]: 15/15 [#566](https://gitlab.com/gitlab-com/gl-infra/production/issues/566) (100%)
      * Merge smaller changes more frequently: &gt; 10 average MRs merged per week: 8.7 average (87%)
    * Secure:
      * Spend no more than 15 point of our [error budget]: 0/15
      * Merge smaller changes more frequently: &gt; 4 average MRs merged per week
    * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere):
      * Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products): No security tool was benchmarked (0%)
      * One CFP (Call For Proposals) accepted for a Tech conference: No CFP accepted so far (0% - to be updated once all answers are there)
    * Monitor:
      * Spend no more than 15 point of our [error budget] : 0/15
      * Merge smaller changes more frequently: &gt; 6 average MRs merged per week : 79% achieved - [Actual weekly average](https://quality-dashboard.gitlap.com/groups/gitlab-org/teams/monitoring) 4.75 MRs
    * [Kamil](https://about.gitlab.com/company/team/#ayufanpl):
      * Improve scalability: Reduce CI database footprint as part of [Ensure CI/CD can scale in future](https://gitlab.com/gitlab-org/gitlab-ce/issues/46499) with [Use BuildMetadata to store build configuration in JSONB form](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21499).
      * Continuous Deployment: Help with building CD features needed to continously deploy GitLab.com by closing 5 [issues related to Object Storage](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name=Object%20Storage) that are either ~"technical debt" or ~"bug"
    * CI/CD:
      * Spend no more than 15 point of our [error budget]: 30/15 [#5375](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5375), [#564](https://gitlab.com/gitlab-com/gl-infra/production/issues/564)
      * Merge smaller changes more frequently: >10 average MRs merged per week
  * Infrastructure: Make GitLab.com ready for mission critical workloads
    * Spend no more than 15 point of our [error budget]: 0/15
    * Deliver streamlined and cleaned-up Infrastructure Handbook
    * SAE: Minimize GitLab.com's MTTR
      * Deliver observable availability improvements in database backup/restore/verification and replication
      * Launch `production` queue management for changes, incidents, deltas and hotspots
    * SRE: Maximize GitLab.com's MTBF
      * Deliver phase 1 DR and HA plan for GitLab.com
      * Implement roadmap and milestone planning with tracking of velocity
    * Andrew: Observable Availability
      * Deliver General Service Metrics and Alerting, Phase 2: error budgets dashboardI and attribution, alert actionability metrics, and MTTD metrics
      * Deliver [Distributed Tracing working in GDK](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4808)
      * Deliver DDoS ELK alerting, documentation and training
    * Delivery: Streamline releases
      * Create release pipelines
      * Create a CD blueprint for 2019
  * Quality:
     * Complete [Review Apps for CE and EE with GitLab QA running](https://gitlab.com/groups/gitlab-org/-/epics/265) => 80%, Completed provisioning infrastructure, remaining polish work captured in [Improving Review App Reliability](https://gitlab.com/groups/gitlab-org/-/epics/605)
     * Complete phase 1 & 2 of [Addressing database discrepancy for our on-prem customers](https://gitlab.com/gitlab-org/gitlab-ce/issues/51438) => 70%, All schema discrepancy identified and documented. We still need to get more data on customer instances but have addressed all issues around missing index or extra index.
  * Support
    * Amer East: Develop at least 2 Premium service process improvements focused on improved coordination with TAMS, Premium customer ticket reduction efforts, and create premium ticket reports that give us more clarity of needs within our Premium customer base.
    * Amer West: Create a *Support Engineer - GitLab.com* Bootcamp more tightly aligned with Production Engineering
    * APAC: TBD process improvement (e.g. SLA performance, Customer Experience, or Employee Development)
    * EMEA: Enable support team to create 30 updates to docs to help capture knowledge and improve customer self service. Track contributions and views to measure 'self service score' (support edited doc visitors / customers opening tickets).
  * UX:
    * Create a seamless cluster experience. Identify 3 additional ways users can create and manage their clusters. Draft a proposal for an MVC that considers product, technical, and user requirements. Push for scheduling and implementation of the first iteration of group and instance clusters in Q4.
    * [Complete a Competitive analysis of version control tools for designers (Abstract, Invision, Figma, etc.). Identify ways GitLab can leverage Open Source and our own tools to assist designers using GitLab.](https://gitlab.com/gitlab-org/gitlab-ce/issues/51654)
    * UX Research
      * [Establish a GitLab beta group and create a standardized process for running beta studies with users. Conduct 1 beta study with users to test and iterate on the process.](https://gitlab.com/gitlab-org/ux-research/issues/100)
  * Security:
      * Deprecate support for TLS 1.0 and 1.1 on GitLab.com: 100% [Blog post](https://about.gitlab.com/2018/12/17/gitlab-tls1011-discontinued-update/)
      * Complete and document 12 month roadmap for ZTN rollout: 100% [Kanban board](https://gitlab.com/groups/gitlab-com/gl-security/-/boards/891046?label_name[]=ZTN)
    * Security Operations:
      * Document at least 5 runbooks for logging sources: X/5, Y%
      * Security and Priority labels on 50 backlogged security issues: X/50, Y%
    * Application Security:
      * Complete at least 2 cycles of security release process: X/2, Y%
      * Conduct at least 3 application security reviews: X/3, Y%
      * Public HackerOne bounty program by December 15: X%
    * Compliance:
      * Document current procedures for each of the 14 ISO Operations Security Domain: X/14, Y%
      * Conduct gap analysis of at least one GitLab.com subnet's access controls: X/1, Y%
    * Abuse:
      * Port existing 'janitor' project to Rails w/ 100% feature parity: X/9, Y%
      * Support at least 2 external requests per month: X/6, Y%
* Product:
  * Significantly improve navigation of the documentation. Ship global navigation. Improve version navigation. Revamp [first page of the EE docs](https://docs.gitlab.com/ee/README.html). => **Total - 90% Complete** - [Ship Global Nav](https://gitlab.com/gitlab-com/gitlab-docs/merge_requests/362) - 100%, [Improve version navigation](https://gitlab.com/gitlab-com/gitlab-docs/issues/248) - 70%, [Revamp First Page](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/7525) - 100%
  * Move vision forward every release. Prioritize at least one vision issue for each stage in every release. => **61% Complete**. Of nine stages - [4 complete, 3 partials and two zeros](https://docs.google.com/document/d/1dAs9HoAbuiRzYYvk-AFMggbN-1Hb1l6L17jMzLePbkA/edit#heading=h.grhsn1edpxk9).
  * Every new feature ships with usage metrics on day 1, without negatively impacting velocity. => **Missed ~0%**. [Significant majority of features shipped without usage metrics on day 1](https://docs.google.com/document/d/1dAs9HoAbuiRzYYvk-AFMggbN-1Hb1l6L17jMzLePbkA/edit#heading=h.896ibct85jd1).
* CMO:
  * Corporate marketing: Help our customers evangelize GitLab. 3 customer or user talk submissions accepted for DevOps related events.
  * Corporate marketing: Drive brand consistency across all events. Company-level messaging and positioning incorporated into pre-event training, company-level messaging and positioning reflected in all event collateral and signage.
  * Corporate marketing: Increase share of voice. 20% lift in web and twitter traffic during event activity.

### CEO: Great team. ELO score per interviewer, executive dashboards for all key results, train director group.

* CMO: Hire missing directors. Operations, Corporate, maybe Field.
* VPE: Craft an impactful iteration to improve our career development framework and train the team => 70%, shipped a new BE eng benchmark and experience factor worksheets (also for SREs) to hopefully address career development and comp for a meaningful proportion of the engineering function.
* VPE: Source 20 candidates by Oct 31 and hire 2 Engineering Directors: 100% (20/20) sourced, hired 1/2 directors => 60%, completed sourcing and hired a Sr Dir.
  * Frontend:
    * Scale process to handle incoming amount ofapplications and hire 2 engineering
      managers and 10 engineers: X hired (X%)
  * Dev Backend:
    * Gitaly:
      * Source 10 candidates by October 15 and hire 1 developer: X sourced (X%),
        X hired (X%)
    * Gitter:
      * Create hiring process for fullstack developer
    * Plan:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: 25 sourced (100%), 2 hired (100%)
    * Create:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: 25 sourced (100%), X hired (X%)
      * Get 10 MRs merged into Go projects by team members without pre-existing
        Go experience: X merged (X%)
    * Manage:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: 25 sourced (100%), 1 hired (50%)
      * Team to deliver 10 topic-specific knowledge sharing sessions
        (“201s”) by the end of Q4 (4 completed)
    * Geo:
      * At least one Geo team member to advance to Geo Maintainer ()
    * Distribution:
      * Source 25 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Stan:
      * [Reduce baseline memory usage in Rails by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49702)
      * [Reduce runtime memory usage in Sidekiq for top 5 workers by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49703)
      * Resolve 3 P1 Tier 1 customer issues
  * Infrastructure: Source 10 candidates by Oct 15 and hire 1 SRE (APAC) manager: X sourced (X%) X hired (X%)
    * SAE: Source 10 candidates by Oct 15 and hire 1 DBRE: X sourced (X%) X hired (X%)
    * SRE: Source 25 candidates by Oct 15 and hire 2 SREs: X sourced (X%) X hired (X%)
  * Ops Backend:
    * Hire 2 engineering managers (1 manager for Release, 1 manager for Secure): Hired 1 (50%)
    * Configure: Complete 10 introductory calls with prospective candidates by Nov 15 and hire 3 developers: 6 prospects (60%), hired 3 (150%)
    * Monitor: Source 30 candidates (at least 5 by direct manager contact) by November 15 and hire 3 developers: >30 sourced (100%), hired 2 (67%)
    * Secure: Source 75 candidates and hire 5 developers: X sourced (X%), X hired (X%)
    * CI/CD: Complete 10 introductory calls with prospective candidates by Nov 15 and hire 2 developers: 0 prospects (0%), hired 0 (0%)
  * Quality:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 test automation engineers: 52 sourced (52%), 2 hired (100%)
  * Security:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 3 security engineers: X sourced (X%), X hired (X%)
  * Support: Source 30 candidates by October 15 and hire 1 APAC Manager
    * Amer East: Source 30 candidates by October 15 and hire 1 Support Agent
    * Amer West: Source 30 candidates by October 15 and hire 1 Support Engineer
    * APAC: Source 30 candidates by October 15 and hire 1 Support Engineer
  * UX:
    * Source 50 candidates by October 15 (at least 10 by direct manager contact) and hire 2 UX Designers and 1 UX Manager: X sourced (X%), X hired (X%)
  * VP Alliances: Scale team to interact with our key partners. Hire Content Manager and Alliance Manager
  * CFO: Improve processes and compliance
    * Legal: Implement a new contracting process to roll out with the vendor management process.
    * Legal: Update global stock option program to ensure global compliance.
  * CFO: Create plan for preparing the Company for IPO
  * CFO: Improve financial performance
    * FinOps: Annual planning process complete with company wide benchmarks and 100% of functions modeled to enable scenario analysis.
    * FinOps: Headcount planning linked to requisition process that provides for recruiting and hiring manager visibility against plan assumptions.
  * CCO:  Improve Recruiting and People Ops Metrics, including relevant benchmarketing data
    * Recruiting: Hiring and Diversity stats and goals defined by functional group
    * Recruiting: Rent Index Evaluation by functional group
    * Recruiting: Single Source of truth for Recruiting data, ownership and progress. If we can get this down to 2 sources in Q4, that will be a win.
    * PeopleOps: Added clarity to the turnover data and industry benchmarketing. Attempt to get benchmark data for remote companies.
  * CCO: Continue on the next iteration of Compensation
    * PeopleOps: Review and revise benchmarks to meet our current Compensation Philosophy
    * PeopleOps: Explore and decide on revising the compensation philosophy that supports hiring and retaining top talent, and have the plan in place to move forward.
    * PeopleOps: Review and revise Rent Indexes to align with the compensation philosophy.
  * CCO: Continue to improve Career Development and Feedback
    * Redesign the experience factor model to focus on supporting development and conduct trainings.
    * Define Cross-Functional and Functional Group specifc VP and Director Criteria
    * Launch an employee engagement survey that results in at least 85% participation and 3 themes to work on as a leadership team.
  * CCO: Improve Recruiting efficiency and effectiveness to build a solid foundation to scale:
    * Clearly define and standardize process, roles, responsibilities, and SLA’s
    * Improve continuity and accountability through recruiter/sourcer/coordinator alignment
    * Launch candidate survey in Greenhouse
    * Implement and integrate new background check provider
    * Enhance candidate communication and templates (invites, updates, decline notices)
    * Optimize Greenhouse and provide refresher training to hiring managers
    * Support Headcount planning process driven by Finance to ensure one source of truth and process for hiring plan and recruiter capacity
    * Continue work on ELO score for interviews. First interation complete by mid November.
  * Product: Hire 2 Directors of Product => **100% Complete** - hired [Kenny](https://about.gitlab.com/company/team/#kencjohnston) and [Eric](https://about.gitlab.com/company/team/#ericbrinkman)

[error budget]: /handbook/engineering/#error-budgets
[phase 1 of preparedness for Elasticsearch on GitLab.com]: https://gitlab.com/groups/gitlab-org/-/epics/429

## Engineering (VPE)

* GOOD
  * Shipped several new impactful salary benchmarks
  * Shipped meaningful career development asset for BE engineers in the form of experience factor worksheets
  * GitLab.com is now meeting business goals
  * Error budgets allowed us to manage risk and solve some problems
* BAD
  * Did not hire the second director of engineering we planned to
  * Did not address comp for every engineering role
  * Did not address career development for every engineering role
* TRY
  * Delegate OKR process for managers to my directors in Q1
  * End error budget experiment in OKRs
  * Only do 2019 Q1 hiring KRs for teams that are not regularly hiring to plan
  * Delegate Director of Engineering hires to Sr Director of Development in Q1
  * Address comp expectations and career development for the remaining roles in engineering in Q1
  * Assume .com continues to meet business goals and not take on the mission critical goal in favor of more granular infra KRs in Q1
  * Arrange training in Q1 for career development facets (like writing secure code) for engineers

### Frontend

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

### Dev Backend

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Plan

* GOOD
  * Met the hiring target
  * No production incidents
  * One more person trained to lead technical interviews, another close
    to finishing their training
  * Single prioritised list works well for the team
* BAD
  * Despite a significant amount of effort towards the Elasticsearch
    goal, we have not made any significant reductions in index disk
    usage
  * Elasticsearch issues have a tendency to get lost or deprioritised
  * One person left the team
  * Although we aren't the most-impacted team, security issues have
    taken a lot of the team's time over the past few months
* TRY
  * Reframing Elasticsearch goal as improving admin controls, reduction
    in downtime during upgrades, progressive enablement, etc. These
    issues allow more incremental progress and don't block improvement
    in index size
  * [Reviewing security issues] to ensure that we are clearing a backlog,
    rather than introducing new issues at the same rate as we fix old
    ones

[Reviewing security issues]: https://gitlab.com/gl-retrospectives/plan/issues/19

#### Distribution

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Geo

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Manage

* GOOD
  * Stayed within the error budget, something we were nervous about after having an incident so early in the quarter.
  * We kept focus on deliverables for much of the quarter (e.g SmartCard, Group SAML, Billing improvements), which feels like an achievement given the size and scope of the team.
  * A decision has been made to create a new team ('Sync'), which will help bring more focus to areas such as Billing & Licensing, while reducing the scope of the Manage team.
  * Made a conscious decision to prioritize reducing the _security_ backlog in the coming milestones.
  * The impact of the 201 sessions (now known as _Deep Dives_) was really positive and has encouraged other teams to do similar.
* BAD
  * Only hired 1 out of 2 positions.
  * Prioritization of security Issues will impact Deliverables until we clear down the backlog.
  * Number of Issues delivered was inconsistent for the 3 milestones in the quarter, making it hard to predict future delivery.
* TRY
  * Work closer with the recruitment team, and other engineering teams, to understand what we need to do to speed up hiring.
  * Experiment with milestone planning to increase both velocity and predictability.

#### Create

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Gitaly

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Gitter

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Stan

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

### Infrastructure

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### SAE

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### SRE

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Andrew

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

### Ops Backend

* GOOD
  * Completed goals for throughput OKR thanks to the support of the team to try a new model and seeing success with it.
  * Hired an engineering manager for release. Very happy to welcome Darby Frey to GitLab!
* BAD
  * Did not hire a manager for secure. This has been a tough role to hire for despite our increased efforts in sourcing and filling the pipeline. We are seeing an uptick in the number of interviews in Q1 likely due to the efforts from Q4 but it's disappointing that we are still in search.
* TRY
  * Work with recruiting to diversify and improve our sourcing for the secure team manager role.
  * Improve our presence at events and help widen our brand awareness on LinkedIn and other recruiting sites

#### Monitoring

* GOOD
  * Sourced more candidates than our goal
  * No error budget points spent
* BAD
  * Only hired 2 out of 3 positions
  * Didn't meet throughput goal (only 79%)
  * Didn't spend any error budget points. This is mostly due to our surface area still being so small.
* TRY
  * Encourage smaller MRs merged earlier. Right now we have a number of small MRs, but they are all getting merged late in the cycle.

#### Verify

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Secure

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Configure

* GOOD
  * Hired more than target, thanks to referrals and introductory calls
* BAD
  * Didn't meet the throughput goal (only 87%)
  * Reluctance to split GitLab issues due to the challenge of splitting our
    communication across multiple issues (significant tradeoff with the
    extensive async commonunication)
  * Not a single hire came from an application (all referral and sourced for
    introductory calls). Not clear if this is a problem since we exceeded our
    hiring target.
* TRY
  * Stress the importance of smaller MRs more also try to make smaller issues
  * Ask for referrals more, encourage team members to share job ads on Linkedin
  * Continue our focus on Introductory calls since 6 calls led to 1 hire which
    is a good return on investment

#### Kamil

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Philippe

* GOOD
  * Several CFPs were posted, I should have at least one accepted.
* BAD
  * Didn't have time to benchmark any tool. It requires to be focused and
    available, which I'm not because of my interim EM position.
* TRY
  * Find a new Secure Engineering Manager to allocate time for my new role OKRs.

### Quality

* GOOD
  * Hired two test automation engineers.
  * Completed Patroni Replication Manager testing with GitLab QA.
  * Completed TLS Depreciation testing with GitLab QA.
  * Completed Rails 5 migration testing wtih GitLab QA.
* BAD
  * An update to the clone / fork button cause GitLab QA tests to break for 4 days.
  * Review Apps are broken (no DNS record) due to an increasing number of DNS records and too many API calls to AWS.
  * We had a few high severity regressions were as a result of Rails 5.
  * We are still lagging behind on test gaps.
* TRY
  * We need to ensure that broken QA tests has the same urgency as a broken master.
  * Incentivize and encourage engineers to write E2E tests.

### Security

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

### Support

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Americas East

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Americas West

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### APAC

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### EMEA

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

### UX

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...

#### Research

* GOOD
  * ...
* BAD
  * ...
* TRY
  * ...
