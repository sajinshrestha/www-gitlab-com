---
layout: markdown_page
title: "FY20-Q1 OKRs"
---

Note: This is the first quarter where we [shift our fiscal year](https://about.gitlab.com/handbook/communication/#writing-style-guidelines). FY20-Q1 will run from February 1, 2019 to April 30, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers (standard implementation path, customer success based on their goals, DevOps maturity), Scalable marketing (Pipe-to-spend for all, double down on what works, more experiments with things like demo's, call us, and ebooks)

* Alliances: Measure the value partners can bring to the business.  3 references for each major public/private cloud, establish customer ability to purchase through marketplaces, joint marketing with key partners - 200+ leads per event
* CRO: Generate IACV from sales to higher tiers.  Target 250 new opportunities created from core accounts, Implement sales motion to show all starter renewal accounts the benefit and value of premium and ultimate, Identify two new already deployed large enterprise core customers in each SAL territory
* CRO: Faster delivery of customer value.  Deliver plan to have Sales quote implementation services and success plan on all new account quotes, Launch standard implementation model in large segment, Measure time to completed implementation for all new enterprise accounts.
* CMO: Recharge Inbound Marketing Engine. Top 10 Gitlab.com Pages optimized for inbound, Inbound trial and contact-us strategy documented and baselined, Pages optimized for top 20 non-branded SEO target terms, Inbound strategy for Dev, Sec & Ops personas launched
    * Content Marketing: Broaden audience reach by increasing new user sessions on the blog by 15%. Create next iteration editorial mission statement and content framework. Publishing plan mapped to key topics, personsa, and campaigns. Publish 5 interactive or deep-dive assets.
    * Content Marketing: Increase social referral traffic 20%. Execute 2 social campaigns. Next iteration social strategy and framework. Create internal social advocacy program.
    * Content Marketing: Build content paths to support lead flow. Create content strategies for 3 personas/buying groups. Execute plan to increase newsletter subscribers 50%. Contribute 10% of overall click-thrus to form fills.
    * Digital Marketing programs: Improve Top 10 Gitlab.com Pages and increase inbound CTR and conversion rate by 10% QoQ.Optimize and increase visibility for 20 non-branded terms, Optimize competition pages for better visibility and to assist competitive take-out with 10% increase in CTR from Organic Google.
    * Digital Marketing programs: 15% QoQ increase in inbound website leads through gated content and paid digital and increase conversion rate with improved nurture. Implement 3 baseline nurture tracks to educate and generate leads within the database, Implement an always-on digital paid program (to include re-targeting, etc.).
    * Product Marketing: Document buyer’s journey (buyer’s experience, tribe needs). Documented handbook pages of messaging around the 3 core pillars of Dev, Sec & Ops.
    * Product Marketing: Execute 1 competitive takeout campaign. Create campaign materials (web pages, demos, whitepapers, blogs), perform global sales enablement sessions.
    * Product Marketing: Identify, evaluate and implement competitor analysis and comparisons strategy. Deliver first 5 comparisons using new strategy.
    * Product Marketing: Improve case study publish rate.  Publish case studies at cadence of at least 2 per month, with at least 3 reference customers using all stages concurrently.
    * Field Marketing: Create the ABM Framework and tooling. Execute ABM in 5 accounts per region, Identify messaging per account, Execute 1 account-targeted online campaign.
    * Community Relations: Increase the number of GitLab CE and EE contributions (not contributors) from the wider community (including Core Team). For the 11.7 release, the target is 180; for the 11.8 release the target is 200; for the 11.9 release the target is 220.
    * Community Relations: Accelerate open source project adoption of GitLab.com. Hire an Open Source Program Manager, gain commitment of 1 big open source project (1000+ contributors) to migrate to GitLab.
    * Community Relations: Jumpstart meetups. Schedule 10 meetup events, at least one in every region (EMEA, APAC, NA).
* CMO: Achieve marketing process and visibility excellence. 100% visibility into any and all leads at any time, Full-funnel marketing dashboard published in Looker, Unified marketing calendar published.
    * Digital Marketing programs: Build out a framework and launch for an integrated campaign structure (ICS), execute one campaign, run one competitive take-out sub-campaign and adapt ICS to ABM use cases.
    * Sales Development: BDR role clarity, defined development path. Clarified Lead Sources, add at least 2 Additional Lead Channels , inbound “call sales” phone line, Drift optimization - implement Drift Account Based Marketing.
    * Sales Development: Reduce xDR Ramp Time to 3 months. Hire an Onboarding and Enablement Manager (Done), “Expert” Certifications around Personas, Product Tiers, better knowledge management solution: battlecards by persona, vertical.
    * Field Marketing: Create Field Marketing Dashboard in SFDC. Identify & create the elements for dashboard, Creation of data driven decision framework for field, Improved visibility of field activity.
    * Field Marketing: Create SLAs around Field Marketing & Marketing Ops. Creation of SLAs for Field Marketing to Ops (ops to field already exists), Create manual dashboard to track SLAs and improve performance, Improve overall speed of contact to MQL cycle time (need baseline).
    * Community Relations: Increase responsiveness and coverage. Decrease median first reply time to 7 hours while adding two new response channels (GitLab forum, Stack Overflow).
* CMO: Achieve $10.85m in IACV marketing sourced pipeline by beginning of Q2 FY19. 720 SAOs delivered to sales by SDRs/BDRs (SAOs as currently defined) @ $12,500 IACV avg, 14,285 MQLS created, hit 60% MQL accepted rate, hit 32% MQL qualified rate.
    * Sales Development: Increase Accepted Opportunity per SDR to 8/month. Rise in reps achieving quota per month. 16 Jan, 18 Feb, 20 Mar, 22 Apr; > 1 significant (3 min+) conversation per rep per day; complete an Outreach.io audit and content improvement.

### CEO: Popular next generation product. Triple secure value (3 stages, multiple teams). Grown use of stages (SMAU). iPhone app on iPad.

* Product: Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, CI/CD for iOS and Android development on GitLab.com, develop a Rails app on GitLab.com web IDE on iPad.
* Product: Increase product depth for existing categories. Consumption pricing for Linux, Windows, macOS for GitLab.com and self-managed, 50% (11 of 22) "new in 2018" categories at `viable` [maturity](/handbook/product/categories/maturity/).
* Product: Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 3 reference customers using all stages concurrently. => [SMAU Dashboard](https://gitlab.looker.com/dashboards/69)
* Tech Writing: Sample 100 support tickets/interactions with customers created in Q1'19 and ensure 100% of all customer facing information used to resolve these issues is incorporated in our documentation in accordance with our [Docs-First Methodology](https://about.gitlab.com/handbook/documentation/#docs-first-methodology).
* Alliances: evaluate qualifying for security competency with major clouds. Joint whitepaper, 1 security reference per major partner
* Alliances: evaluate if key partner services (AKS,EKS,GKE, VKE, etc) can grow use of stages.  Conversion of SCM customers to CI/CD through joint GTM.
* CRO:  Increase CI usage.  Perform CI workshop/whiteboard at 2 of the top 10 customers that are only using SCM today in each region/segment by end of Q1.
* VPE
  * Development: Increase velocity. Increase throughput 20% for all teams.
    * Dev Backend: Communicate and address pain points with issue boards:
      identify and work with Product to prioritize 5 issues
    * Dev Backend: Increase ownership for key GitLab components: add one
      new maintainer for each of Gitaly, Pages, Workhorse,
      and the Elasticsearch indexer
      * Engineering Fellow: Reduce memory consumption for GitLab: address 2 main
        consumers in gitaly-ruby, reduce ProjectExportWorker usage by 50%
      * Engineering Fellow: Make multithreaded server ready for production:
        istrument with Prometheus metrics, prepare production readiness review
      * Geo: support Production team in preparing Geo for DR: enhance selective
        sync to work at scale, support GitLab Pages
      * Manage: Take on more maintainer responsibilities: add a CE/EE backend maintainer
      * Create: Improve maintainability and performance of GitLab Shell: port GitLab Shell to
        Golang
      * Create: Take on more maintainer responsibilities: add a CE/EE backend
        maintainer, add a database maintainer
      * Plan: Prepare Elasticsearch for use on GitLab.com. [Improve admin
        controls](https://gitlab.com/groups/gitlab-org/-/epics/428),
        [reduce index size](https://gitlab.com/groups/gitlab-org/-/epics/429)
    * Ops Backend: Increase BE teams's throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       * Verify: Increase Verify throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       * Release: Increase Release throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       * Configure: Increase Configure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
       * Monitor: Increase throughput and predictability, Increase throughput by 20% and even out throughput over the release cycles to bring the standard deviation below 2 for the most recent 12 weeks. As of Jan 4 the standard deviation is [2.8](https://www.wolframalpha.com/input/?i=standard+deviation+of+2,7,8,5,9,8,1,5,5,3,1,3)
       * Secure: Increase Secure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
      * [Kamil](https://about.gitlab.com/company/team/#ayufanpl):
          * Train 2 new maintainers of GitLab Rails,
          * Help with building CD features needed to continously deploy GitLab.com by closing 5 that are either ~"technical debt" or ~"bug"
      * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Handover to new Secure Engineering Manager.
      * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): [CISSP](https://www.isc2.org/Certifications/CISSP) certification.
      * [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products).
    * Ops Backend: Define additional data to complement throughput: Implement a prototype for team pulse and define a path to implement it in GitLab product.
    * Dev Frontend + Backend: GA of GraphQL to increase througput for BE and FE. Resolve general topics like performance, abuse prevention and enjoyable development experience. Endpoints for Create and Plan relevant models. Implement new frontend features in Create and Plan only with GraphQL.
    * Frontend: Increase throughput and reduce implementation time of Frontend features by 20%. By having at least 50 gitlab-ui components at the end of Q1. Increase velocity of CSS adaptions. Improve tooling in GitLab itself for JS engineering (webpack map, visual diffing, etc.).
  * Frontend: Improve Performance of JS computation that is done on every page by 10%. Improve Initialisation execution by 20%. Reduce main bundle size by 20%.
  * Infrastructure: Make all user-visible services ready for mission-critical workloads while continuing to seamlessly support delivery of new product functionality. [Availability] Definition and tracking of GitLab.com metrics for SLAs, [Product] Deliver OKR Infrastructure workflow and tooling using GitLab, [Introspective] Achieve a 10% cost reduction on GitLab.com spending.
    * SRE[AS]: Support SRE-at-large MTBF and MTTR objectives. [Availability] Reduce spin-up time for provisioning auto-scaled servers to <3.5min, [Product] Automate error budgets as a GitLab feature, [Introspective] Deliver new testing environment.
    * SRE[DS]: Drive all user-visible services' MTBF to Infinity. [Availability] Deliver improvements to GitLab.com reliability on two foundational subsystems (ZFS on storage, Kubernetes + Charts), [Product] Take full operational ownership of CI/CD (take oncall from tomscz), [Introspective] Operationalize Vault and Consul.
    * SRE[JF]: Drive all user-visible services' MTTR to less than 60 seconds. [Availability] Implement 2 significant availability features to improve database reliability (autovacuum, masterless backups), [Product] Deliver Patroni on GitLab, [Introspective] Deliver full service operational inventory
    * Delivery: Streamline GitLab deployments on GitLab.com and self-managed releases. [Availability] Create deployment rollback process, [Product] Reduce CE/EE pipeline runtime by 20%, [Introspective] Replace `takeoff` off with a method better suited for CI pipelines.
    * Andrew Newdigate: Improve observability for all-user visible services to maximize MTBF and minimize MTTR. [Availability] Deliver distributed tracing on GitLab.com, [Product] Deliver Puma on GitLab and experiements on GitLab.com, [Introspective] Deliver overall service-level tracking in preparation for SLA tracking.
  * Quality: Build everything in GitLab. Move GitLab Insights production ready graphs into GitLab itself.
  * Quality: Improve GitLab performance. Continous measure of GitLab memory usage based on real user scenarios via functional performance tests.
  * Quality: Add Engineering Productivity metrics. Rollup dashboard for MR review time, MR size, rolling average time to resolution of P1 and P2 issues.
  * Security: Secure the Product. Red Team with H1.
  * Security: Secure the company. Evaluate at least 2 enterprise centralized SSO solutions and make selection. Evaluate at least 2 CBT-based secure coding training courses and work with People Ops to incorporate as part of developer onboarding process.
  * Support: Evolve Service Levels by plan and customer segment to improve Customer Experience, measured by First Reply Time achievement and Customer Satisfaction.
  * Support: Drive creation of reference architecture for 10k GitLab install
  * Support: Enhance documentation through modified workflows and team execution to include ‘doc change’ first for each (or some reasonable % of ‘each’) ticket resolution.
  * UX: Improve the onboarding experience for new GitLab users. Increase week 1 retention by X%, Create onboarding journeys for each of our personas.
  * UX: Drive adoption of unknown/unused features. Create user journeys/flows for each stage group and determine X ways to introduce connections between them, Design a framework for onboarding users to unused features that increases adoption by 15% and can be reused across the application.
  * UX: Make GitLab usable by everyone. Introduce accessibility testing to gitlab-ui/csslab, Fix X accessibility issues outlined in our VPAT.


### CEO: Great team. Employed brand (known for all remote, great communication of total compensation, 10 videos per manager and up), Effective hiring (Faster apply to hire), ELO score per interviewer), Decision making effectiveness (kpis from original source and red/green, training for director group)
* CFO: Improve financial reporting and accounting processes to support growth and increased transparency.
    * Manager of Data Team: 100% of executive dashboards completed with goals and definitions
    * Manager of Data Team: Public release of finance metric(s).
    * Director of Business Operations: Data Integrity Process (DIP) completed for ARR, Net and Gross Retention and Customer counts.
    * FinOps Lead: Integrated financial model that covers 100% of expense categories driven by IACV (as first iteration) and marketing funnel (as second iteration).
    * FinOps Lead: Release and change control process for financial reporting documented and added to handbook.
    * Sr Dir. of Legal: Detailed SoX compliance plan published internally and reviewed by Board.
    * Sr Acctg Manager:  Key internal controls documented in handbook.
* CFO: Create scalable infrastructure for achieving headcount growth
    * Sr Dir. of Legal: 90% of team members covered by scalable employment solution
    * Payroll and Payments Lead: Automatation of contractor payroll completed.
* CFO: Improve company wide operational processes
    * Director of Bus Ops: Roadmaps for all staffed business operations functions are shipped and Q2 iteration by EOQ
    * Controller: Zuora upgrade to orders which will allow for ramped deals, multiple amendments on single quote and MRR by subscription reporting.

* VPE: Address compensation concerns. Develop a compensation roadmap document to effectively set expectations for every role in engineering, hold roundtable meeting for each role benchmark
    * Ops Backend: Define first iteration for BE role framework (Software Engineer, Senior Software Engineer, Staff Software Engineer)

* VPE: Invest in our existing team members. Unify vacancy descriptions and experience factor content, develop career matrices for all roles
  * Development: Deliver the 2019 roadmap. Hire IC's to plan, Hire Directors for secure and enablement
  * Development: Improve customer focus. Get every engineer in one customer conversation
    * Dev Backend: Improve documentation and training for supporting our
      customers: identify and address 3 major pain points for customer support
      and/or professional services.
      * Engineering Fellow: Help solve critical customer issues: Solve 1 P1
        issue
      * Distribution: GA for GitLab Operator: GitLab Operator enabled by default
        in the charts
      * Distribution: Create automated charts installation and upgrade pipelines for supported Kubernetes flavors (e.g. GKE, EKS, etc.)
      * Gitter: Improve security practices: Enable use of dev.gitlab.org for
        security issues, document security release process for gitter.im in
        partnership with Security
      * Gitaly: Make rapid progress on top company priorities: ship GA of object
        deduplication, ship beta of Gitaly HA
      * Manage: Increase test coverage for the customers app: increase from 45%
        to 60%
      * Manage: Proactively reduce future security flaws. Identify five areas
        where we have systematic/pervasive/repeated security flaws, Finalize
        plan to tackle three areas, 2 merge requests merged
      * Create: Increase set of people working on Create features: have team
        deliver 6 Create Deep Dives
      * Plan: Highlight recent work, particularly backstage. Four blog posts
        from the team on recent Plan work.
      * Distribution: Generate library licences and collection for the official Helm charts (similar to the omnibus-gitlab functionality)
  * Ops Backend: Improve onboarding for new engineers: Improve team pages, resources, time to first MR merged by new engineers: Goal for first MR to be complete in the first 2 weeks.
      * Verify: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      * Release: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      * Configure: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      * Monitor: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
      * Monitor: Foster engineer growth. Each engineer has a career plan and measureable goals for the next 6 months: 0/5 engineers
      * Secure: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.

* CCO: Improve the candidate experience at GitLab. This will require all members of the interview team to prioritize interviewing and inputting interview feedback quickly, in addition to process improvements and proper staffing of the recruiting team and wll be measured by a reduction to the time a candidate is in process to 30 days from application to Offer and 80% for all interviewers inputting their feedback within 24 hours.

* CCO:  Improve GitLab's training and development progress, by hiring an L&D specialist (by the end of Q1), increasing the tuition remimbursement utilization by 10%, and defining metrics for each training that inform on the value of the training (80% of attendees rate the training as valuable and/or actionable, key business improvement metrics that should be impacted by the training, follow-up survey 30-60 after training to guage applicablility). The improvement should also be reflected in the annual Engagement survey.

* CCO:  Improve GitLab's Employer brand, starting with hiring a Employer Branding specialist, identifying 5-10 locations to focus our branding and sourcing (25% increase in pipeline from these locations); collaborating with Marketing to ensure that recruiting activities are integrated into at least 95% for conferences, and increasing our responsiveness to Social Media posts (respond to 80% for Glassdoor posts)
