---
layout: markdown_page
title: "Department Structure"
---

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in BambooHR. Please [check out the Team Page](/company/team/org-chart) to see our org chart.

## By Function - Where GitLabbers Report 

| Sales                | Marketing           | Development      | Product | G&A                 |
|:--------------------:|:-------------------:|:----------------:|:-------:|:-------------------:|
| Business Development | Corporate Marketing | Meltano          | PM Dev  | Business Operations |
| Channel              | Demand Generation   | Infrastructure   | PM Ops  | CEO                 |
| Commerical Sales     | Field Marketing     | Development      |         | Finance             |
| Customer Solutions   | Pipe-to-Spend       | Quality          |         | People Ops          |
| Customer Success     | Product Marketing   | Security         |         | Recruiting          |
| Enterprise Sales     | Outreach            | UX Research      |         |                     |
| Field Operations     |                     | Customer Support |         |                     |


## Allocation Methodology

GitLab uses an indirect cost allocation. GitLab allocates various cost items throught its departments based on consumption of resources or headcount. Below are the departments or diagrams that illustrate how various cost items are allocated at GitLab.


### By Cost Center
<table>
  <tr>
    <th>Sales</th>
    <th>Cost of Sales</th>
    <th>Marketing</th>
    <th>R&D</th>
    <th>General & Administrative</th>
  </tr>
  <tr>
    <td>Business Development</td>
    <td>Customer Solutions</td>
    <td>Corporate Marketing</td>
    <td>Meltano</td>
    <td>Business Operations</td>
  </tr>
  <tr>
    <td>Channel</td>
    <td>Customer Support</td>
    <td>Demand Generation</td>
    <td>Development</td>
    <td>CEO</td>
  </tr>
  <tr>
    <td>Commerical Sales</td>
    <td></td>
    <td>Field Marketing</td>
    <td>Quality</td>
    <td>Finance</td>
  </tr>
  <tr>
    <td>Customer Success</td>
    <td></td>
    <td>Pipe-to-Spend</td>
    <td>Security</td>
    <td>People Ops</td>
  </tr>
  <tr>
    <td>Enterprise Sales</td>
    <td></td>
    <td>Product Marketing</td>
    <td>UX Research</td>
    <td></td>
  </tr>
  <tr>
    <td>Field Operations</td>
    <td></td>
    <td>Outreach</td>
    <td>PM Dev</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2" align="center">Infrastructure</td>
    <td>PM Ops</td>
    <td></td>
  </tr>
  <tr>
      <td colspan="5" align="center">Recruiting</td>
  </tr>
  <tr>
      <td colspan="5" align="center">Business Operations Owned, Companywide Expenses (i.e. gmail, slack, zoom)</td>
  </tr>
</table>

