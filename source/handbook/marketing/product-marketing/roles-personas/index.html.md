---
layout: markdown_page
title: "Roles and Personas"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab personas and enterprise IT roles

### Roles vs personas

Personas describe the ideal target for GitLab. They help us define our messaging and marketing delivery. They are theoretical people to target. By defining their concerns and where they go for information, we can best spend our marketing dollars and sales efforts by focusing on this ideal target.  

Roles are distinct job titles. These are the real people you will encounter while selling. You will find a contact at an account with a specific role. Understanding the challenges faced by each role in IT, along with what they care most about, is helpful to  deliver the right value proposition to the right person.

### Personas
"Personas" are a generalized way of talking about the people we communicate with
and design for. Keeping the personas in mind with their problems and pain points
allows us to use language that the personas understands. GitLab has both buyer
and user persona types.

### Writing for all personas
- When writing content, always remember to use the [GitLab voice](https://gitlab.com/gitlab-com/marketing/general/blob/master/content/editorial-style-guide.md) regardless of the persona.
- Never alienate other personas. You can appeal to a buyer without buzzword bingo.
- Look at who uses a channel most (twitter & docs: user, webinar linkedin: buyer) when shared (website) guide them /features for users /solutions for buyers.

### User personas
User personas are people who actually use GitLab. They may or may not be the person in
the organization who has the authority and budget to purchase Gitlab, but they
are heavy influencers in the buying process. User personas are kept in the [GitLab Design System](https://design.gitlab.com/getting-started/personas).

- [Full-stack web developer](https://design.gitlab.com/getting-started/personas)
- [DevOps Engineer](https://design.gitlab.com/getting-started/personas)
- [Junior web-developer](https://design.gitlab.com/getting-started/personas)
- [Security analyst](https://design.gitlab.com/getting-started/personas/#persona) (uses the Security Dashboard)

### Buyer personas

Buyer personas are the people who serve as the main buyer in an organization or
a champion within an enterprise the drives the buying conversation and
coordinates various teams to make a purchase.

1. Director DevOps [video ](https://www.youtube.com/watch?v=qyELotxsQzY), Director DevOps [slide deck](https://docs.google.com/presentation/d/1x-XUhAXkZxl8ZGe4ze9qcWMmrWITc7es1fYNY_OHQQA/) or Director of IT
1. VP IT [video](https://www.youtube.com/watch?v=LUh5eevH3F4), VP IT [Slide Deck](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/) or VP of Engineering
1. Chief Architect [Video](https://www.youtube.com/watch?v=qyELotxsQzY), Chief Architect [slide deck](https://docs.google.com/presentation/d/1KXsozYkimSLlEg3N-sKeN7Muatz_4XimYp-s7_dY1ZM/) or CIO
1. Chief Information Security Officer or Director of Security (may fund Ultimate upgrade for security capabilities)

Also see [the four tiers on our pricing page](https://about.gitlab.com/handbook/ceo/pricing/#four-tiers).


### Enterprise IT roles we sell to

While personas describe ideal targets, roles are the real people in job titles you will encounter while selling. Understanding the challenges faced by each role in IT, along with what they care most about, is helpful to  deliver the right value proposition to the right person. It will help you know:   
    a) if they are even someone who may have an interest in GitLab (don't waste time),  
    b) what questions to ask to learn more and qualify the lead,  
    c) what value prop they'll want to hear that could get them to a demo, discussion, or POC.  

See the [Enterprise IT Roles page](./enterprise-it-roles/) for more details about who we sell to and how to approach them.
