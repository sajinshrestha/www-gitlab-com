---
layout: markdown_page
title: "Code Contributor Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
Use the following template to create an issue in the [Contributor Program group](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program) about 5 weeks prior to the scheduled event.

## Prior to the Hackathon

### Hackathon minus 4 weeks
1. [ ] Publish a blog post to announce the event (incl. prizes).
2. [ ] Promote the event & the blog post on social channels.
   1. [ ] [Gitter](https://gitter.im/gitlabhq/community)
   2. [ ] [Forum](https://forum.gitlab.com/)
   3. [ ] [Reddit](https://www.reddit.com/r/gitlab/)
   4. [ ] [Twitter](https://twitter.com/gitlab)
3. [ ] Add the new event to the [GitLab Hackathon page](https://about.gitlab.com/community/hackathon/).
4. [ ] Add the new Hackathon to the [GitLab events page](https://about.gitlab.com/events/) as a "featured" event.
5. [ ] Add/update the Hackathon banner on the [Contribute page](https://about.gitlab.com/community/contribute/). 

### Hackathon minus 2-3 weeks
1.  [ ] Line-up topics and speakers for tutorial sessions.
2.  [ ] Update the [GitLab Hackathon page](https://about.gitlab.com/community/hackathon/) with event details such as tutorial sessions. 
3.  [ ] Post reminders on social channels.
  
### Hackathon minus 1 week
1.  [ ] Post reminders on Slack channels (e.g. #core, #mr-coaching).
2.  [ ] Create an issue to track MR's during the event.  [An example from Q4'18](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon/q4-2018-hackathon/issues/1)
3.  [ ] Update social channels highlighting event logistics including schedule, logistics, etc.  

## During the Hackathon
1.  [ ] Announce the event kickoff on social channels.
2.  [ ] Post recorded tutorial sessions on YouTube.
3.  [ ] MR's
    1. [ ] Add the submitted MR's to the issue create above to track MR's. 
    2. [ ] Triage the MR's by applying labels and mentioning appropriate product managers/groups for the MR.
4.  [ ] Throughout the event, monitor Gitter and other channels for questions/discussions. If necessary escalate any questions to other GitLab team members (e.g. #mr-coaching on Slack).
5.  [ ] At the end of Day 1, post a wrap-up tweet with accomplishments highlighted.
6.  [ ] At the end of the event, post an event wrap-up tweet and thank the community.

## After the Hackathon
1.  [ ] Post an event wrap-up on social channels.
2.  [ ] Post a thank you message on #thanks Slack channel. 
3.  [ ] Remove the event as a "featured" event in the [events page](https://about.gitlab.com/events/)
4.  [ ] Delete/update the Hackathon banner on the [Contribute page](https://about.gitlab.com/community/contribute/). 
5.  [ ] Give a remider to the community on the deadline for merged MR's and who they can contact if they need any assistance with MR's. 
6.  [ ] Open an issue to annouce the prize winners and list their GitLab ID's only (no email addresses due to privacy concerns). If any of the winners want to provide a different email address (vs. what is in the commit), they can open a Service Desk issue so they can provide their emails confidentially.  [An example from Q3'18](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon/q3-2018-hackathon/issues/6)
7.  [ ] Post a wrap-up blog post (incl. prize winners).
8.  [ ] Update the [GitLab Hackathon page](https://about.gitlab.com/community/hackathon/) with links to prize winners and the wrap-up blog post. 