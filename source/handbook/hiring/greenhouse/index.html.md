---
layout: markdown_page
title: "Greenhouse"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## For all Greenhouse users

### How to join Greenhouse

[Greenhouse](www.greenhouse.io) is GitLab's ATS (Applicant Tracking System). All hiring managers and interviewers will use Greenhouse to review resumes, provide feedback, communicate with candidates, and more.

You will receive an email to log in to Greenhouse for the first time. When you [log in to Greenhouse](https://app2.greenhouse.io/users/sign_in), choose "Sign in with Google," making sure you're already logged into your GitLab email account. All GitLab team members are granted basic access, which will allow them to add and track referrals and share job postings on social media. Until their access is upgraded by an admin, they will only have access to one page in Greenhouse, [Dashboard](https://app2.greenhouse.io/dashboard), where employees can make referrals. If you need your access to be upgraded so you can participate in the hiring process for a vacancy, please reach out to Recruiting.

### Your dashboard

The Greenhouse [dashboard](https://app2.greenhouse.io/dashboard) will be your main hub for activity when in Greenhouse. All team members have access to this page, but depending on your role in the hiring process you may have more contents on this page than others. All team members can see the "Add a Referral", "My Referrals", and "Share Jobs with your Social Network" section (all of which are described in more detail below). On the righthand panel, there is a section called "Helpful Links" which will bring you to various links across Greenhouse, as well as the Greenhouse support team.

Members of a hiring team may have additional sections, such as "My Interviews", which contains a snapshot of your next three scheduled interviews, with a link to the candidate's profile and interview kit; you can click "See All Interviews" to view your full list of upcoming interviews and "See Past Interviews" to view your completed interviews. You may also see "My Follow-up Reminders" section, where any upcoming reminders for you will sit.

Recruiters will see an additional section "Applications to Review" which will have a list of their roles with a link to the application review functionality, where they can quickly review applications and advance, decline, or submit feedback. This panel shows only the five roles with the most pending applications, but you can click "See All" and choose a role that you are responsible for beyond this list. Hiring managers do not have this section on their dashboard; however, they can access application review by going to the job dashboard for their vacancy and clicking on "Application Review" on the righthand panel.

On the righthand panel, members of the hiring team can see a list of "My Tasks" and "All Tasks" which shows how many candidates you have a pending action item on, such as "Needs Decision", "Candidates to Schedule", "Take Home Tests to Send", or "Offers". You can view the full list of pending tasks by clicking "All Tasks".

Members of the approval process (i.e. executives, Finance, and People Ops) will have an additional section titled "My Approvals" which will contain a list of any candidates or new vacancies that are pending their approval.

There is also a quick link to "People I'm Following" so if you are following any candidates you can quickly get to their profile. To follow a candidate, go to their profile and click "Follow" on the right side.

The last button of note is the "Personalize Dashboard" button at the bottom of the righthand panel. You can use this to adjust your settings on your dashboard to hide or rearrange sections to streamline your workflow.

### Making a referral

All GitLab team members can [make a referral](https://support.greenhouse.io/hc/en-us/articles/201982560-Submit-Referrals-from-the-Dashboard) directly on the [Greenhouse dashboard](https://app2.greenhouse.io/dashboard) by pressing the "Add a referral" button. You do not need to select an "Office" as this will limit the jobs you're able to select in the list. Please complete the required fields, as well as **being sure to include their email address**. We request you either upload their resume using the upload button at the top left or include a link to their LinkedIn profile, or we will reach out to you to get this information.

If your referral has already sumitted their application, ping the recruiting team in the #recruiting channel on slack and they will update the applicant's profile in Greenhouse. To protect the applicant's privacy, please do not include thier name in your message in the #recruiting channel. A recruiter will reach out to you to get that information. 

You can stay updated on the status of your referrals in the referral section of your dashboard, where it will show each referral's name, the role they were referred for, the stage they are in, and the last contact date. If you have any questions regarding your referral, please reach out to the recruiting team, but please note that the recruiting team is unable to provide any feedback to the referrers, only to the candidates themselves.

### Sharing vacancies on social media

If you would like to [share a job posting](https://support.greenhouse.io/hc/en-us/articles/200721674-Generate-My-Referrer-Link) with a particular person or on social media, you can do it via the "Share your link with people interested in working at your company" option in Greenhouse below the "Add a referral" button. Simply choose the role you wish to share, and copy and paste the link.

You are able to share any or all of our current vacancies through social media by connecting your Greenhouse account with your social media accounts, such as LinkedIn and Twitter.

To connect to your social networks, go to your [dashboard](https://app2.greenhouse.io/dashboard) and scroll to the `Share Jobs with your Social Network` section, and click the options to connect to the different platforms. Once you click that button, it will redirect you to the social platform and ask if you allow Greenhouse to post updates and view your basic profile information (you can withhold this at any times in either your Greenhouse or social media settings). Once you accept, it'll bring you back to the Greenhouse dashboard.

Under the social network section, you'll now see options to share the vacancies. To post one job right now, click "Post" next to the platform you want to post on, choose the job you want to share, and feel free to adjust the language in the share message. There is a pre-established template, but we encourage you to personalize it in order to attract applicants!

You can also [schedule posts](https://support.greenhouse.io/hc/en-us/articles/200925999-How-do-I-schedule-posts-to-my-social-networks-) posts to your social media sharing jobs by clicking the "Schedule Posts" button. You can choose as many jobs as you like here, choose when you want to share them (the cadence, days, and times), then adjust the language in the message, and click "Schedule". Don't worry if you make a mistake or want to change something; you can customize or delete individual posts once they are scheduled, and there will be a list of your scheduled posts in your social network section on Greenhouse.

You can see the total number of applicants that have applied through a social network link, either one that you have generated to share or one through a scheduled social network post, by logging into Greenhouse and scrolling to the "Share Jobs with your Social Network" section. Next to each social network you've connected, there will be a number showing how many posts you've made, how many candidates have applied, how many are currently active, and how many have been hired. Please note these candidates do not count towards the referral program and will not show up under your "My Referrals" section, so if you have a specific candidate you want to refer, please be sure to follow [the steps above](https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral) in order to refer them, or reach out to the recruiter for the role to make sure you are listed as the referrer and so you can give any feedback or context you have regarding your referral.

## For all interviewers

### Feedback and interview kits

When you have an interview, you have two ways to get to your interview kit to leave feedback for a candidate. The first is by clicking the link in the calendar invitation for the interview. The second is by going to your Greenhouse [dashboard](https://app2.greenhouse.io/dashboard) where you will see a list of your upcoming interviews and can click "See Interview Kit".

The interview kit consists of a few tabs:
- Interview Prep
  - Quick overview of what you'll want to look for in the interview, as well as any notes that previous interviewers had that they want you to look into further.
- Job Details
  - The full vacancy description posted on Greenhouse.
- Resume
  - This tab will only show up if the candidate provided a resume.
- LinkedIn
  - This tab will only show up if the candidate provided their LinkedIn profile.
- Scorecard
  - This is where you will enter all of your notes, score various attributes, and make a decision. (More detail below!)

Additionally, the interview kit shows the candidate's name, contact details, pop-out links to their resume, cover letter, and other details, as well as the details for the interview (location, time, etc.) on the lefthand side. It also includes the role the candidate is interviewing for at the top of the page.

#### Scorecards

The scorecard consists of a few elements. There are text boxes where you can add in your notes. Some scorecards for different roles or stages may have additional text boxes for specific questions. If a text box is required, there will be a red asterisk by the question.

Underneath the first text box "Key Take-Aways", there are two additional links, `Private Note` and `Note for Other Interviewers`, which will open additional text boxes if you click on them. Private notes are typically only used by the recruiting team when collecting compensation information and are only viewable by the recruiting team and hiring managers for the role. The **note for other interviewers** text box is extremely useful to all interviewers, as this is a place where you can include any information that you think would be relevant in future interviewers, including areas to dig into, further evaluate, or look out for. Any notes you put in this text box will appear in the next interviewers' interview kit on the "Interview Prep" tab.

Below the text boxes for notes, each role has a list of attributes that we are looking for. Each stage in the process has certain attributes highlighted that it's recommended for you to evaluate; however, no attributes are required, so you can feel free to rate any attributes you've gained insight into. All stages include attributes for values-alignment, which all interviewers are heavily encouraged to complete, so we can assess values-alignment for each candidate.

Below the attributes is the final piece where you will make your decision. Greenhouse says "Overall Recommendation: Did the candidate pass the interview?"; this should be interpreted as "Do you want to hire this candidate?" and answered appropriately. The final score is not required by Greenhouse, but it must be completed by the interviewer. If the interviewer does not add their vote, the recruiting team will follow up with them to include their vote. If you are on the fence about a candidate, that is typically an indication that you do not want to hire the candidate, but if you are really unsure feel free to reach out to your recruiter to discuss further. Your recruiter may agree with your hesitations and decline, or you may agree that there is an element that should be further explored in an additional interview.

Your scorecard will automatically save as you enter information, and record that it saved it the top righthand corner. If there is an error and Greenhouse is not able to save the scorecard, it will say so at the top righthand corner and you'll be unable to submit your scorecard. You can either wait until it does save, or open the interview kit again and copy/paste the information over. If you have further issues, please reach out to the recruiting team.

Finally, click `Submit Scorecard` and you're done! From there, you may go back to review and edit your scorecard, view the candidate profile, or return to your dashboard.

In order to help remove bias, interviewers (unlike hiring managers) are not able to see others' scorecards until after their own scorecard has been submitted. If there are certain flags or notes that should be highlighted to the rest of the interview team, the interviewer should add a note in their scorecard by clicking the "Note for Other Interviewers" section right underneath the "Key Take-Aways" text box in their scorecard. You are also able to add notes outside of your scorecard that will be visible to future interviewers by going to the candidate profile and to the text box on the right side under "Make a Note", adding your note, and checking off the radio button to make it "Visible to Interviewers". 

#### Interview notifications

You can set up reminders in Greenhouse by going to your [account settings](https://app2.greenhouse.io/myinfo) and turning on `Daily interview reminder email` which will email you each morning with a list of your interviews for the day. You can also connect your Slack account to your Greenhouse account and receive reminders after the interview is over if your scorecard is still due.

### Searching candidates

Since we have a great pool of talent within our ATS, Greenhouse enables the hiring teams to search for specific attributes of candidates. To search for keywords, go to the candidates tab at the top of the Greenhouse website, use the search bar on the lefthand sidebar to input your keywords, and enable to option "Full Text Search".

A popular method of searching is [using boolean queries](https://support.greenhouse.io/hc/en-us/articles/202360199-Search-Candidates-Using-Boolean-Queries-) which allows you to combine keywords or phrases to get more relevant results, such as `engineer AND "ruby on rails"`. All fields in a candidate resume, application, forms, and more are searchable, and you're able to filter even further using Greenhouse's filter tools on the sidebar.

### Email syncing

All email communication with candidates must be kept in Greenhouse. Only Job Admins (hiring managers, executives, and people ops) are able to email candidates from Greenhouse; if you are not a job admin and want to email your candidate, please reach out to your recruiter. In order to ensure that the candidates' responses are also kept in Greenhouse, you have two options, as there is no automatic sync. The first option is to use a [Google Add-on "Greenhouse for Gmail"](https://support.greenhouse.io/hc/en-us/articles/360003111031-Greenhouse-for-Gmail-Google-Add-on-) which will allow you to sync any emails you receive from within your Gmail inbox. The second option is to either bcc your outgoing email or forward your received email to `maildrop@ivy.greenhouse.io`.

### Leaving notes

It's always recommended to leave notes in a candidate profile to maintain communication between the hiring managers, interviewers, and recruiting team. To leave a note, go to a candidate profile, and on the right side under "Make a Note" type your note, tagging anyone you want to view this note. If you do not tag anyone, no one will receive a notification and it may be lost. Once you're done with your note, click "Save" and it will notify anyone tagged and be logged in the candidate's profile under "Activity Feed", where anyone who has access to the role can view it.

Private notes should only be used to discuss compensation or other confidential items related to the candidate, but this information should always be kept in private notes. To leave a private note, go to the candidate profile, click "Private" under their name, scroll to the bottom and click "Add Private Note". Be sure to tag anyone you want to be notified, but be aware that only Job Admins (i.e. not interviewers) are able to view private notes. All Job Admins including hiring managers are able to see private notes whether or not they are tagged.

### Access to roles

If you need access to review or interview candidates for a vacancy, please reach out to the recruiting team, either through Slack or email. The recruiting team will verify with the hiring manager for the vacancy, and then provision access accordingly.

## For Hiring Managers

### What access do I have?

As a hiring manager or executive, you have access to view all candidates for any vacancies you are marked as a `Hiring Manager` for, as well as all private notes for any of those candidates. While interviewers are restricted in that they are not able to see previous notes or feedback for a candidate when they are scheduled for an interview, hiring managers do not have that restriction and are able to see all notes and feedback at any point in the process. Hiring managers are not able to view candidates anymore once they are hired, or any candidates outside of the role they are a hiring manager for.

### How do I view my roles and candidates?

When you log in to Greenhouse, you will see your dashboard which will not paint the full picture of your roles. In order to view the status of your current vacancies, click "All Jobs" at the top of the page. Then choose whichever job you want to review. There will be an indicator if you are the hiring manager for a role or not below the job name, and you will only be able to view the jobs you have access to as a member of the hiring team for that role; please note the below details are only accessible to Job Admins, and Interviewers have less functionality available to them.

You will be brought to the job dashboard for that specific job, which contains all of the information you should need. At the top of the page, there is a quick snapshot of application trends, showing you how many applications you've received, how many have been rejected, and how many are active. Below that you can see a snapshot of the sources where you are getting your applicants as well as the average quality of the applicants from each source. Next, you'll see a list of any candidates you're following in this role. The last section on this page is "Pipeline Tasks" where any pending tasks for candidates in this role will be shown. You can also view a less detailed version of this task list at the top right sidebar on this page, where you can easily click "Review Applications" (which is the quickest and easiest way to bulk review applications) or click on the number next to each stage to view the candidates in each stage. If you wanted to view all candidates regardless of stage, simply click "Candidates" right above the "Application Trends" section (**not** the "All Candidates" at the very top of the page). Below the snapshot pipeline at the right sidebar, you can view any prospects for this role, Greenhouse predictions of when the role will be filled, and finally job setup links that enable you to adjust the settings for the vacancy. It's recommended for only the recruiting team to edit the vacancy setup, but please reach out to them if you have any questions.

If you want to review the candidates for multiple jobs at the same time, click "All Candidates" instead, and then use the filter on the left sidebar, `Jobs` > `Filter by Job` > select the appropriate jobs. You can use the filters to further narrow down candidates.

### Configuring notifications

Notifications are configured as part of the [vacancy creation process](https://about.gitlab.com/handbook/hiring/vacancies/#open-the-vacancy-in-greenhouse) but can be adjusted at any time. To set up notifications, go to the vacancy's job dashboard and click on "Job Setup" at the top, then "Notifications" at the left. Under each section, you can then click "Edit" to add or remove yourself for that section.

### Reporting

Hiring managers have the ability to quickly view and pull reports for their vacancies. To do so, go to the job dashboard of the vacancy you want to report on. Then, click "Reports" below the vacancy name. From there, it will show you a variety of different reports you can pull depending on what you're looking for, and each one will dynamically show you the report which you are able to use some filters on. You're also able to [save reports](https://support.greenhouse.io/hc/en-us/articles/115003218066-Saved-Reports) to come back to them later, [share a report via email](https://support.greenhouse.io/hc/en-us/articles/115003338503-Email-a-Report) to [set up recurring reports](https://support.greenhouse.io/hc/en-us/articles/115003711906-Schedule-Recurring-Reports) to have reports emailed to you or someone else on a weekly basis, and to download the report as an excel file for easy sharing through Google Sheets.

### Application Review

To access application review for your candidates, go to the job dashboard for the vacancy you want to review. On the right sidebar, under `Pipeline`, you can see how many applicants are in application review and click "Review Applications" to be taken to a portal that shows each candidate's name (which is an active link that will open a new tab to the applicant's profile), application date, answers to application questions, resume, cover letter, and any other details of their application. The application review will show you the applications with the least recent activity (whether that is their application date or any activity on their profile took place), so you can work your way from oldest to newest. You are able to advance, skip, or reject applicants, as well as leave feedback with any of those decisions. It's recommended to always include feedback when reviewing applications so the recruiting team is aware of your thought process. It is perfectly acceptable to skip candidates you are not sure of and tag your recruiter in the feedback section for them to review and take action on. If you do advance an applicant, be sure that your recruiter either receives notifications when candidates change stages or tag them in feedback and ask them to move forward (the safest option). If you [reject a candidate](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates), always be sure to send the candidate an email using one of the templates. You can also "skip" the candidate and leave feedback for the recruiter to reject, which is recommended.

To view the application questions of a candidate outside of the application review portal, go to their profile, click "Application" on the left side, scroll down and expand "Job Post Questions". If there are no job post questions for the candidate, it is likely they were sourced or exported from an external job board, so you can feel free to send them the job post to answer these questions.

## For Recruiting

### Access levels and permissions

Greenhouse has a large variety of various permissions in order to make sure each person has access to the right things. Different people should be assigned different permissions, which are described in detail below.

- The **recruiting team** (with the exception of the Recruiting Director, Talent Operations Specialist, and Diversity Sourcing Specialist, outlined below) should always have the access level `Job Admin: Recruiting` for all closed, open, and future jobs, regardless of their involvement with a particular vacancy (e.g. if they are an interviewer or coordinator).
- Everyone in the **people ops team** (with the exception of the CCO and recruiting team) should always have the access level `Job Admin: People Ops` for all closed, open, and future jobs, regardless of their involvement with a particular vacancy (e.g. if they are an interviewer).
- All **executives** (with the exception of the CEO and CCO) should always have the access level `Job Admin: Job Approval` for all of closed, open, and future jobs *in their department* (this gives them all the same access as a hiring manager and also the ability to approve new jobs), regardless of their involvement with a particular vacancy (e.g. if they are an interviewer or hiring manager).
- All **hiring managers** (with the exception of executives, which are listed above) should always have the access level `Job Admin: Hiring Manager` for all closed, open, and future jobs *in their department*. The exceptions to this are: 1) if they are a hiring manager for only one role within a department, do not give them access to future jobs, only the current vacancies they are a hiring manager for, and 2) if they are interviewing for another vacancy outside of their own team, then give them `Interviewer` access for that vacancy only (while keeping the `Job Admin: Hiring Manager` access level for their own vacancies).
- **Executive Assistants** should always have the access level `Job Admin: Scheduling` for all closed, open, and future jobs, regardless of their involvement with a particular vacancy (e.g. if they are an interviewer).
- Anyone who needs to have access to all jobs and data (including diversity data) or be an administrator for the account should be a `Site Admin`. Currently, the team members who are Site Admins include the CEO, CCO, CFO, Senior Director of Legal Affairs, Recruiting Director, Talent Operations Specialist, and Diversity Sourcing Specialist.
- Anyone who is on an interview team or reviewing applications for a vacancy who does **not** fall into one of the above categories should have the access level `Interviewer` for _only_ the vacancies they need access to and, if applicable, future vacancies in their department (e.g. if they are an interviewer for all Security vacancies, they would be added as an `Interviewer` for all current and future Security vacancies, but nothing else). This is the lowest permission level besides `Basic` so make sure to not give hiring managers this level.

There are a couple of other permission levels available in Greenhouse, but that are not frequently used.
- `Job Admin: Standard`: this permission level came with Greenhouse and is not customizable to our needs
- `Job Admin: Limited`: this permission level has the basic permissions of a Job Admin (which are "Can see dashboard, pipeline, and reports", "Can see all candidates and submitted scorecards", "Can add and edit candidates and referrals", and "Can be assigned hiring team roles") with no other permissions; use rarely and only when needed

#### How to upgrade or change access levels

Only Admins can upgrade another team member's access level. The default is "Basic", but anyone reviewing and interviewing candidate profiles should be upgraded to the "Interviewer" role. To do this:
 1. Go to [settings](https://app2.greenhouse.io/configure) and click on [users](https://app2.greenhouse.io/account/users?status=active)
 1. Search for the team member in the search bar; if their name appears, click on their name
 1. Click "Edit" in the permissions section. Choose the "Job Admin/Interviewer" option
 1. Click "Add" under "Job-Based Permissions"
 1. Search for the job they should have access to and click "add" next to it, then choose "Interviewer"

For any team members who are hiring managers (or above), choose "Job Admin: Hiring Manager" for the specific roles they should have access to; if they are a hiring manager for an entire department, they should also be added as "Job Admin: Hiring Manager" for future roles within that department. If they are a hiring manager or above for a role, they should always be marked as "Job Admin: Hiring Manager" and _not_ "Interviewer" even if they will be interviewing, since that will limit their access. Meanwhile, the executive for a division (e.g. CRO, CMO, VPE, etc.) should receive "Job Admin: Job Approval" for all of their division's current and future roles in order to give them the same permissions as "Job Admin: Hiring Manager" as well as the ability to approve new vacancies.

Similarly, Recruiting receives "Job Admin: Recruiting" for all current and future roles; People Ops receives "Job Admin: People Ops" for all current and future roles.

There are additional "User-Specific Permissions" listed beneath the job-based permissions.
1. For hiring managers and above, always check off "Can create new jobs and request approvals" with the default role being whatever access level they would normally have (e.g. hiring manager is "Job Admin: Hiring Manager" but an executive is "Job Admin: Job Approval"), as well as "Can create new job stage names (Job Admin only)".
1. For the people ops team, check off "Can create new jobs and request job approvals", "Can invite new users to Greenhouse and reactive disabled users", "Can manage unattached prospects", and "Can invite and deactivate agency recruiters".
1. For the recruiting team, check off each additional option.
1. Site Admins should typically have access to all of the additional options, but *always* be sure to check off "Can see EEOC and demographic reports", "Can create and view private candidates", and "Can see private notes, salary info, manage offers, and approve jobs/offers".

### Configuring notifications

Notifications are configured as part of the [vacancy creation process](https://about.gitlab.com/handbook/hiring/vacancies/#open-the-vacancy-in-greenhouse) but can be adjusted at any time. To set up notifications, go to the vacancy's job dashboard and click on "Job Setup" at the top, then "Notifications" at the left. Under each section, you can then click "Edit" to add or remove people for that section.

It is recommended for the Recruiter of a vacancy to set up notifications for themselves regarding new internal applicants, new referrals, new agency submissions, approved to start recruiting, offer fully approved, stage transitions, and new scorecards.

For stage transition and new scorecards, it is possible to select `Candidate's Recruiter` or `Candidate's Coordinator` instead of a particular person for each stage, which is highly recommended.

### Configuring email permissions

Greenhouse allows Job Admins to grant other Job Admins the permissions to send out emails on their behalf. This is helpful, for example, when a recruiter is sending out an availability request email but wants the responding email with the times to go to the coordinator so they can schedule next interviews, they could send the email from the coordinator's email address to ensure that happens. In Greenhouse, the activity feed on the candidate's profile shows who actually sent it and who it came from, so that it is clear.

In order to set up these permissions, the person allowing another person to send emails on their behalf should log in to Greenhouse, hover over their name in the top right corner, and click "Account Settings". Then scroll down to the section called "Email Permissions". You can select the first option to allow any Job Admin or Site Admin to send emails on your behalf, or you can select the second option and choose specific people (who are Job Admins or Site Admins) only who are able to send emails on your behalf. You can remove these users or this functionality at any time.

Once you have the permissions to send emails on someone else's behalf, when you are sending an email to a candidate, you can click the "From" button at the top of the email pop up and choose from the list of available email addresses. If you are using an email template, remember to choose the template and then change the sender, or the template will override any changes you've made.

### Changing jobs

If a candidate applies for a job but is a better fit for another job, they will need to be added to a new job. There are a few options to do this; to start, go to the candidate profile and click "Add or Transfer Candidate's Jobs" at the bottom right. Fro there, you can either add a new job to the candidate profile, which will keep all data within the original job and start a clean slate for the new job. Alternatively, you can also transfer the candidate data to the new job, but this should be done with caution, as it will not transfer any scheduled (i.e. not completed and submitted) interviews or scorecards, and it will remove the candidate from any reports about the original job. Super Admins are also able to remove a job and its history from a candidate profile, which should be done only when absolutely necessary.

### Auto-tags

It is possible to add tags that automatically link to applicants when they apply based on their answers to certain application questions. To add an auto-tag to a vacancy, go to the vacancy's "Job Setup" page, then "Job Post". To the right of the vacancy name, there will be a link "Manage Rules". Click this, then click "Add a Rule". You can then select which application question on this vacancy and what answer you want to create the auto-tag for. (Please note: only application questions that are yes/no, single select, or multi-select may have auto-tags.) For example, you could choose the question "Do you have experience with Ruby on Rails?" and when an applicant answers "No" from the dropdown options. Then check off "Tag" and it will populate all current tags to choose from, such as "auto-reject", "no ruby experience", "needs visa", "no-hire country", etc. Once you've selected any and all tags you want to be associated with this question and answer, click "Save". If you want to create auto-tags for multiple questions and/or multiple answers, you will need to repeat this process.

Going forward, all new applicants whose answers in their application questions correlate to the rule you set up will automatically also have the tags you chose. To quickly find candidates, go to either "All Candidates" or the candidates for a specific vacancy, open the "Profile Details" section on the left sidebar, and under "Candidate Tag" select the appropriate tag. You can add additional filters as desired, and you can then perform bulk actions on these candidates by using the bulk function on the top right.

### Scheduling interviews with Greenhouse

The recruiting team will schedule most interviews, which is a three-step process.

To schedule an interview, first make sure to move the candidate to the correct stage (Screening, Team Interview, Executive Interview, etc.), then request the candidate's availability by pressing the `Request Availability` button when viewing the stage. It is recommended that you modify the email with the general work hours of the interviewer for smoother scheduling. You are also able to provide suggested times that the interviewer is available by quickly pulling up their calendar within Greenhouse and selecting times. If you need to re-request availability (for rescheduling, scheduling a new interview in the same stage, etc.), click where it now says one of "Requested", "Received", or "Confirmation Sent" and choose "Request Availability". The button to request availability will now re-appear.

Once the candidate provides their availability, you will receive an email and can schedule the interview by clicking the "Schedule Interview" button on the candidate's profile next to the appropriate interview stage. Choose the interviewer and click the "Find Times" button at the top right, and find a free spot on the interviewer's calendar (making sure it is within their working hours). Note both the candidate's and interviewer's timezone for easier scheduling. To change the calendar's timezone, you can click settings and click the drop-down with your timezone. Greenhouse will show the available times of the interviewee in white on the calendar. If you do not find a suitable time on the first day, you can change days by clicking the arrows near the current day. Note that Greenhouse **does not** currently update timezones once you change days, so you will need to reselect the timezone again even if it looks like it is selected in the timezone bar. When scheduling, be sure to not schedule more than 3 interviews per day for an interviewer. If you are not able to see their calendar, you can either check the "Interview Calendar" in Google Calendar to view if they have any other interviews that day or reach out to the interviewer to confirm. The only exception to not scheduling more than three interviews per day is if you are scheduling screening calls for a recruiter.

When sending out the calendar invites to the interviewers, the recruiting team will include a link to a Zoom room for *all* interviewers under the "location" field, which will follow this format: `https://gitlab.zoom.us/my/gitlab.firstnamelastname` (e.g. Joe Smith's would be: `https://gitlab.zoom.us/my/gitlab.joesmith`). This room will be each interviewer's consistent location for their interviews, and the naming convention is standard across GitLab for anyone with a Pro Zoom account. All interviews will be conducted via Zoom to maintain a streamlined hiring process. To create a personal Zoom Room for someone, please follow the [instructions in the handbook](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro).

Once a time is selected, you'll need to send the candidate an interview confirmation. You can do so by clicking the "Send Interview Confirmation" button, which will bring up an email template. Note that the timezone defaults to your timezone even on the candidate's side. Therefore, it is recommended to either do the conversion for the candidate to avoid confusion or highlight that it is in your timezone, not necessarily theirs. There are [timezone tools](https://www.worldtimebuddy.com/) you can use for timezone conversions. Be sure to update the template to the correct Zoom room for the interviewer, and always select the `Include calendar files` button below the email and then click `Send Email`.

Please be sure to include the role for both the interviewer and the interviewee so that each side is aware of who they will be speaking with. When sending out the calendar invite to the interview, the role the candidate is interviewing for should automatically populate in the title, but be sure to double check. When sending the interview confirmation email to the candidate, be sure to include the title of the person they will be meeting with, as this is not automatic.

At this time, it is not possible to bulk schedule candidates; however, it is helpful to filter the candidates under `Pipeline Tasks` > `To be scheduled` so you can see everyone who needs to be scheduled on the page and easily scroll through them.

#### Important scheduling and interview notes

* Please be sure to go to your [Google Calendar settings](https://calendar.google.com/calendar/r/settings?tab=mc) and input your current timezone.
* Make sure that a candidate is in the proper stage to schedule when scheduling.
* If you cannot attend the interview, please let the recruiting team know so they can reschedule. **Simply declining the interview does not effectively notify the recruiting team.** The recruiting team should first cancel the currently scheduled interview through either Greenhouse or Google Calendar (specifically the `Interview Calendar`) before creating the new one to avoid any confusion.
* All outgoing emails and scheduling must be done through Greenhouse. Scheduling details, emails, and communication between team members can be viewed in the activity feed of a candidate.
* Timezones can be tricky with Greenhouse. Always double check timezones before sending confirmations, noting that all interview confirmations and the time shown on the candidate's profile is in your own timezone.  
* You can find a candidate's timezone by viewing their application.
* To ensure that someone else (another candidate, for example) does not join your video call without your permission, log into your Zoom account online. Click on "My Account" in the top-right corner, and click on "Meeting Settings" in the left sidebar. Scroll down to "In Meeting (Advanced)," and toggle on the option "Waiting Room."

### Rejecting candidates

To remove a candidate from the active pipeline, they need to be rejected in Greenhouse, which is typically done only by recruiting. Any time a candidate is rejected, the recruiting team will email them letting them know, as they will not automatically be notified if we reject them. When clicking the reject button on a candidate, Greenhouse will open a pop up where you can choose the appropriate rejection reason, as well as a rejection email template. Feel free to adjust the template per the [guidelines in the handbook](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates). You can also select a time delay to send out the rejection email. Finally, you are also able to start a new prospect process for a candidate when rejecting them, in the event you want to reach out to them again in the future.

If you want to consider a candidate for the future, the best practice is to archive them with the reason "Future Interest"; however, you can also start a prospect process when rejecting them so they are including in a pool of future interest candidates for the role, or you can simply add a follow-up reminder for yourself, the recruiter, and/or the coordinator to reach out to the candidate in a certain amount of time.

Please ensure that if candidates are rejected or remove themselves from the process that all further scheduled interviews are cancelled either through Greenhouse or through the Google Calendar "Interview Calendar" in order to remove the calendar event from interviewers' calendars. Rejecting a candidate will not automatically remove the calendar event from an interviewer's calendar.

You can also reject candidates in bulk by going to the [candidates page](https://app2.greenhouse.io/people), filtering accordingly, clicking `Bulk Actions` at the top right, selecting the appropriate candidates or by clicking "Select All", clicking "Edit Selected", clicking "Reject" in the pop up, and following the same procedure as above to reject and email. Note: you can only choose one reason when archiving in bulk.

To unarchive a candidate, click "Unreject" on the candidate profile under the appropriate role.

### Changing stages for a candidate

Each role has similar stages, with various interviews within each stage. Below is a typical outline of the stages:

- Application Review
  - This stage typically consists only of one event, which is the application review, where all new applicants are added. Hiring managers and/or interviewers can leave a scorecard in this stage on if they want to advance or reject a candidate.
- Assessment (added per role)
  - This stage is only added to roles that use questionnaires as part of the interview process. The assessment is done completely within Greenhouse, where the questions are added per job, the candidate submits their answers via a link in Greenhouse, and the graders review the answers in Greenhouse and leave a scorecard.
- Screening
  - This stage has only the screening call, where the recruiting team does an initial call with the candidate to review their experience, skills, and values-alignment.
- Team Interview
  - The "Team Interview" usually has several interviews within it, typically with the hiring manager, two peers, a director, and/or a panel interview.
- Executive Interview
  - This stage consists of two interview events, one for the executive of the division and another for the CEO. Both of these interviews are optional.
- Reference Check
  - There are two spots for completed reference checks, one from a former peer and one from a former manager.
- Offer
  - This is where the recruiting team prepares the offer and the offer is approved, and is explored more in detail in the [interviewing section](https://about.gitlab.com/handbook/hiring/interviewing/#offer-package) of the handbook.

On rare occasion, there may be additional or less stages than represented here, but these stages should be consistent as much as possible in order to maintain data integrity for reporting. The interviews within the stages can be adjusted as needed, as long as they follow the same names (e.g. there should only be one `Peer Interview 1` across all jobs and not a `Peer Interview 1` on one job and a `Peer Interview One` on another). If there is any doubt or confusion, feel free to reach out to the Talent Operations Specialist.

If a candidate will have more interviews in a stage than predetermined, you can add additional interview events as long as the candidate is in the stage where you need to add the additional event.

### Updating candidate information

To update a candidate source, go to the candidate's profile and, underneath the name of the job they're being considered for, click the pencil button. Where it says "Source", search for the correct source; some common examples are "Referral", "LinkedIn (Ad Posting)", "LinkedIn (Prospecting)", "Internal Applicant", and "Jobs page on your website". If you need a source that is not listed and you're not able to add a new source, reach out to the recruiting team. If you select the referral option or one of the outbound sourcing (also called prospecting) options (such as "LinkedIn (Prospecting)"), be sure to then select "who gets credit" by searching for the referrer or sourcer who found the candidate. Then click "Update Source". If they were referred or are an internal applicant, their profile will now have a highlighted field next to their name indicating this.

To update a candidate's personal details, go to the candidate's profile and click "Edit Profile" on the top right. You'll be able to change the candidate's name, current company, current title, tags, phone numbers, email addresses, social media accounts, websites, address, and education details. You can also go to the "Details" tab of their profile and click "Edit" next to "Info" as these fields are connected.

To update a candidate's assigned recruiter and coordinator, go to the candidate's profile and go to the "Details" tab, then scroll down to where it says "Source & Responsibility". Click the pencil icon that will appear when hovering over either the recruiter or coordinator, then update accordingly. (This can also be done in bulk if needed using the bulk actions button on the candidates page.) You can update their source in the "Source & Responsibility" section as well.

To add attachments or documents to a candidate's profile, go to their profile and to the "Details" tab, then scroll to the bottom where it says "All Attachments". Next to the appropriate job they are being considered for, click "Add file" and choose if it is a resume, cover letter, or other document, then click "Choose File" and upload the document from your computer. You can change the visibility of a document by clicking the three dots (...) above the document so that only Super Admins and Recruiting can view it. Please note that cover letters and resumes are by default public to the hiring team for that vacancy and cannot be made private.

### Updating or adding new departments

Occasionally, the Finance team updates our organization's divisions and departments. We try to keep Greenhouse as aligned as possible to Finance's structure so that we can maintain accurate alignment and headcount planning.

Please note that only Site Admins are able to update or add new departments in Greenhouse. In order to update or add divisions or departments, log in to Greenhouse and go to the [configure](https://app2.greenhouse.io/configure) section by clicking the gear at the top right corner. Then click ["Organization"](https://app2.greenhouse.io/account/organization) and scroll down the the "Departments" section. You will see each current division listed and, when clicking on the expand button, each department that falls under that division.

To change the name of a division or department, click the pencil button next to it. Please note you are unable to have a division and department with the same name. To add a new division or department, click "New Department" and type in the name; if it is a new division, simply click create, but if it is a new department click the "Subordinate to" dropdown and choose the division it is associated to (for example, the "Security" department is a subordinate to the "Engineering" division).

There are currently two exceptions to the official divisions and departments, in order to keep things organized and clear for both applicants and reporting purposes. Firstly, "Customer Success" is its own division with subordinate departments instead of falling under "Sales". Secondly, "Engineering Management" is a department under "Engineering" instead of being portioned out to various teams.

## Additional resources

### Trainings

Internal GitLab trainings were done on Greenhouse, for basic users, interviewers, and hiring managers. These trainings can be found in the [GitLab Videos folder](https://drive.google.com/drive/u/1/folders/1IK3Wb3P9_u0akMx5ASG26cuehY_LeRe8) on the Google Drive and are only accessible to GitLabbers, as there is confidential information contained within the videos.

### Greenhouse support

Greenhouse has a very robust [support center](https://www.greenhouse.io/support), with articles, how-to videos, webinars, and more.

If you have a product request or want to submit a support ticket, you can reach out to the recruiting team, or you can reach out to Greenhouse support directly by emailing `support@greenhouse.io`, using the Live Chat option on your Greenhouse dashboard, or submitting a request on [Greenhouse support's website](https://support.greenhouse.io/hc/en-us/requests/new).
