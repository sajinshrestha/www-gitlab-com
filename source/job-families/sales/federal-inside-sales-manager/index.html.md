---
layout: job_family_page
title: "Federal Inside Sales Manager"
---

## Responsibilities

Federal Inside Sales Manager

 * Federal Inside Sales Manager will report to a Federal Sales Director.
 * Lead team (SDR, BDR) to ensure healthy weekly activity: calls, leads, demos, and opportunity progression (MEDPIC)
 * Review weekly forecast and business outcomes with reps and sales leaders.  This includes win/loss analysis so we can iterate approach based on our success and failures.
 * Manage sales order processing including monitoring the system to ensure accurate receipt booking of sales orders, tracking sales shipment, delivery.
 * Responsible for all renewals of all contracts and upgrades
 * Work with Federal Channel Manager on teaming on call campaigns, Quotes on RFQ through distribution, Interacting with individual sales reps on specific/important accounts to ensure progress and appropriate messaging.
 * Manage Reseller relationships: Quoting, Lead generation, Renewals, Product and price list updates,
 * Getting GitLab EE on necessary federal contracts (e.g. NETCENTS)
 * Own documentation for Fips 140.2 documentation, FedRamp Documentation and VPAT updated with UX team
 * Work with Federal contract officers to ensure RFQ’s are responded and closed in a timely manner. This would include deciding which partners could bring value if we assign them to run with an account.
 * Work with sales team and partners to leverage success with small implementations at SIs (LMCO, GD, Raytheon, etc) into enterprise level discussions to use GitLab EE for all .gov projects
 * Training/mentoring of team members to ensure open feedback, career development and job satisfaction
 * Improve and maintain Salesforce structure for accurate sub accounts (groups/programs), opportunities and contacts.
 * Collaborates regionally and globally in developing/enhancing standardized processes, reports, and sales programs that facilitate efficient sales operations, effective sales execution, and improved management insight.  This includes ensuring the GitLab sales handbook is followed and/or modified when necessary.

## Requirements

* A true desire to see customers benefit from the investment they make with you
* 5+ years of experience with B2B software sales
* Experience selling into large organizations
* Interest in GitLab, and open source software
* Ability to leverage established relationships and proven sales techniques for success
* Effective communicator, strong interpersonal skills
* Motivated, driven and results oriented
* Excellent negotiation, presentation and closing skills
* Preferred experience with Git, Software Development Tools, Application Lifecycle Management
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
  1. What would differentiate you and make you a great account executive for GitLab?
  1. What is your knowledge of the space that GitLab is in? (i.e. Industry Trends)
  1. How do you see the developer tools changing over the coming years from a sales perspective? (i.e. Competitive Positioning, Customer Needs, etc)
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with Federal Sales Director
* Candidates will be invited to schedule a second interview with our CRO
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Compensation

You will typically get 50% as base and 50% based on commission. See our
[market segmentation](/handbook/sales/#market-segmentation) for
typical quotas in the U.S.
Also see the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/).
